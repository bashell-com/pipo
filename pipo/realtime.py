import subprocess
import sys


def run_command(command, shell=False, verbose=False):
    """
    Execute `command` on subprocess and print output in realtime.
    """
    out = ''
    if verbose:
        print('command: %s' % command)
        sys.stdout.write(' output: ')
    try:
        process = subprocess.Popen(
            command if shell else command.split(),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            shell=shell,
            bufsize=1,
        )
    except Exception:
        code = -500
    else:
        while True:
            try:
                out = process.stdout.read(1).decode()
            except:
                pass
            else:
                if (not out) and (process.poll() is not None):
                    break
                if verbose and (out == '\n'):
                    sys.stdout.write('\n output: ')
                else:
                    sys.stdout.write(out)
                sys.stdout.flush()
        code = process.returncode
    if verbose:
        if out != '\n':
            print('')
        print('   code: %d' % code)
    return code

