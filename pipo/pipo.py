import asyncio
import subprocess
from datetime import datetime
from multiprocessing import Process
from multiprocessing import Queue


async def tick(callable_function, delay=1, executor=None, loop=None):
    if loop is None:
        loop = asyncio.get_event_loop()
    while True:
        loop.run_in_executor(executor=executor, func=callable_function)
        await asyncio.sleep(delay)


def run_command(command, timeout=5, shell=False, verbose=False):
    """
    Execute `command` on subprocess with the given `timeout`.
    """
    try:
        process = subprocess.run(
            command if shell else command.split(),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            timeout=timeout,
            shell=shell,
        )
    except subprocess.TimeoutExpired:
        output = ''
        code = -500  # process running time is exceed given timeout.
    else:
        output = process.stdout.decode()
        code = process.returncode
    if verbose:
        print('verbose output'.center(79, '-'))
        print('command: %s' % command)
        print(' output: %s' % '\n         '.join(output.rstrip().split('\n')))
        print('   code: %d' % code)
    return output, code


def call_function(callable_function, timeout=5, verbose=False):
    """
    Call `callable_function` with the given `timeout`.
    """

    def wrap(callable_function, queue):
        queue.put(callable_function())

    queue = Queue()
    process = Process(target=wrap, args=(callable_function, queue))
    process.start()
    pid = process.pid
    if verbose:
        print('process #%d started at %s' % (pid, datetime.now()))
    process.join(timeout)
    if process.is_alive():
        process.terminate()
        process.join()
        if verbose:
            print('process #%d is terminated at %s' % (pid, datetime.now()))
    else:
        result = queue.get()
        if verbose:
            print('process #%d return: %s at %s' % (pid, result, datetime.now()))
        return result
