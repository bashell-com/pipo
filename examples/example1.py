#!/usr/bin/env python
import sys; sys.path.append('..')
import asyncio
from functools import partial
from pipo import *


'''
An example to use Pipo's tick as a scheduler which
always run given task at a specific time delay loop.
'''

loop = asyncio.get_event_loop()

# Task#1 run `sleep 10` to sleep for 10 seconds, but with 3 sec timeout.
task1 = partial(run_command, command='sleep 10', timeout=3)

# Task#2 display sleep processes
task2 = partial(run_command,
                command="ps a | grep 'sleep\|PID' | grep -v 'grep'",
                shell=True, verbose=True)

# Run task#1 and task#2 in parallel, with 1 sec delay loop.
# With 3 sec timeout, we should not have more than 3 sleep processes running.
loop.run_until_complete(
    asyncio.gather(
        tick(task1, delay=1),
        tick(task2, delay=1),
    )
)
loop.close()
