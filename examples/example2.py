#!/usr/bin/env python
import sys; sys.path.append('..')
import asyncio
from functools import partial
from pipo import call_function
from pipo import tick
from time import sleep
from random import randint


'''
An example to use Pipo's tick as a scheduler which
always run given task at a specific time delay loop.
'''

loop = asyncio.get_event_loop()

def slow_calculation(delay):
    a = randint(0, 10)
    b = randint(0, 10)
    print('--> start slow calculattion %d + %d' % (a, b))
    sleep(delay)
    return a + b

# Task#1 run long calculation a+b for 10 seconds, with 3 sec timeout.
slow_calculation_10 = partial(slow_calculation, delay=10)
task1 = partial(call_function,
                callable_function=slow_calculation_10, timeout=3, verbose=True)

# Task#2 run long calculation a+b for 2 seconds, with 3 sec timeout.
slow_calculation_2 = partial(slow_calculation, delay=2)
task2 = partial(call_function,
                callable_function=slow_calculation_2, timeout=3, verbose=True)

# 3 sec timeout
loop.run_until_complete(
    asyncio.gather(
        tick(task1, delay=1),
        tick(task2, delay=1),
    )
)
loop.close()
